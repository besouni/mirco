<?php
    $name_of_table = "users";

    $create =  "CREATE TABLE IF NOT EXISTS $name_of_table (
                id INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
                mail VARCHAR(30) NOT NULL,
                rule_id int(10) UNSIGNED,
                password VARCHAR(30) NOT NULL,
                reg_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                FOREIGN KEY (rule_id) REFERENCES rules(id) ON DELETE RESTRICT ON UPDATE RESTRICT
    )";
    $conn->exec($create);
?>