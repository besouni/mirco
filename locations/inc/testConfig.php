<?php
require_once 'inc/testFunctions.php';
spl_autoload_register('myLoader');

const APP_NAME = 'Locations';

/**
 * The APIs
 */
const API_XML_1 = 'https://data.rtr.at/api/v1/tables/ffat.xml';
const API_XML_2 = 'https://data.rtr.at/api/v1/tables/MedienFrequenzpool.xml';
const API_XML_3 = 'https://data.rtr.at/api/v1/tables/skp.xml';
const API_DRIVING = 'https://data.wien.gv.at/daten/geo?service=WFS&request=GetFeature&version=1.1.0&typeName=ogdwien:FAHRSCHULEOGD&srsName=EPSG:4326&outputFormat=json';
const API_MEDICAL = 'https://data.wien.gv.at/daten/geo?service=WFS&request=GetFeature&version=1.1.0&typeName=ogdwien:ARZTOGD&srsName=EPSG:4326&outputFormat=json';
const API_CHURCH = 'https://data.wien.gv.at/daten/geo?service=WFS&request=GetFeature&version=1.1.0&typeName=ogdwien:RELIGIONOGD&srsName=EPSG:4326&outputFormat=csv';
