<?php
    require_once 'inc/testConfig.php';
    $source = $_GET['source'] ?? 'xml-1';
    switch ($source) {
        case 'driving':
            $data = new GeoJSON(API_DRIVING);
            break;
        case 'medical':
            $data = new GeoJSON(API_MEDICAL);
            break;
        case 'church':
            $data = new CSV(API_CHURCH);
            break;
        case 'xml-1':
            $data = new GeoXML(API_XML_1);
            break;
        default:
            $data = new GeoJSON(API_DRIVING);
            break;
    }
    $data->read();
    echo "<pre>";
//        print_r($data->locations);
    echo "</pre>";
?>
