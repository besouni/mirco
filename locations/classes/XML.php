<?php
class XML extends Webservice {
    public function read(?string $filter = null)
    {
        $kmldata = simplexml_load_file($this->url) or die("Error, konnte nicht geladen werden");
        // TODO: exception handling, falls die API nicht erreichbar
//        var_dump();
//        echo "<pre>";
//        print_r($kmldata->Document->Folder->Placemark);
//        echo "</pre>";

        foreach ($kmldata->Document->Folder->Placemark as $placemark){
//            echo "<pre>";
//            print_r((float)$placemark->LookAt->longitude);
//            echo "<hr>";
//            echo "</pre>";
            $name = (string) $placemark->description;
            $name = explode("\n", htmlspecialchars_decode($name));
//            echo "<pre>";
//            print_r($name[2]);
//            echo "<hr>";
//            echo "</pre>";
            $name = strip_tags($name[2]);
            $longitude = (float) $placemark->LookAt->longitude;
            $latitude = (float) $placemark->LookAt->latitude;

            $this->locations[] = new Location($name, $longitude, $latitude);

        }
    }
}