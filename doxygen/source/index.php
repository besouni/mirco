<?php
    $n1 = 34;
    $n2 = 56;

//    This is a addition function
    function addition(float $n1, float $n2) : float{
        return $n1 + $n2;
    }

//    This is a subtraction function
    function subtraction(float $n1, float $n2) : float{
        return $n1 - $n2;
    }

//    This is a multiplication function
    function multiplication(float $n1, float $n2) : float{
        return $n1 * $n2;
    }

//    This is a division function
    function division(float $n1, float $n2) : float{
        return $n1 / $n2;
    }
?>